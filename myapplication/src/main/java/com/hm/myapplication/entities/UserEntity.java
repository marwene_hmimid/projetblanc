package com.hm.myapplication.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;


@Entity
@Table(name = "users")
@Inheritance(strategy = InheritanceType.JOINED)

public class UserEntity extends MyEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String email;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean isManager;
	@Enumerated(EnumType.STRING)
	private RoleEntity roles;
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean enabled;
	
//	@ManyToMany
//	@JoinTable(
//	  name = "company_user", 
//	  joinColumns = @JoinColumn(name = "user_id"), 
//	  inverseJoinColumns = @JoinColumn(name = "company_id"))	
//	List <CompanyEntity> company;
	
	
    @OneToMany(mappedBy = "userEntity")
    Set<CompanyUserEntity> companyUserEntity;
	
	
	
	public UserEntity(Long id, String email, String username, String password, String firstName, String lastName,
		boolean isManager, RoleEntity roles, boolean enabled, Set<CompanyUserEntity> companyUserEntity) {
	super();
	this.id = id;
	this.email = email;
	this.username = username;
	this.password = password;
	this.firstName = firstName;
	this.lastName = lastName;
	this.isManager = isManager;
	this.roles = roles;
	this.enabled = enabled;
	this.companyUserEntity = companyUserEntity;
}

	public Set<CompanyUserEntity> getCompanyUserEntity() {
		return companyUserEntity;
	}

	public void setCompanyUserEntity(Set<CompanyUserEntity> companyUserEntity) {
		this.companyUserEntity = companyUserEntity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isManager() {
		return isManager;
	}

	public void setManager(boolean isManager) {
		this.isManager = isManager;
	}

	public RoleEntity getRoles() {
		return roles;
	}

	public void setRoles(RoleEntity roles) {
		this.roles = roles;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}






	public UserEntity() {
		super();
 	}
	
	
	
	
	
	
	
	
	
	
	
 
}
