package com.hm.myapplication.service;

import java.util.List;

import com.hm.myapplication.dto.UserDTO;
import com.hm.myapplication.entities.UserEntity;

public interface UserService {

	public List<UserDTO> findAllUsers();

	public void addUser (UserDTO user);
	
	public List<UserEntity> getAllManagers();
}
