import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GestionDeStockComponent } from './gestion-de-stock/gestion-de-stock.component';
import { InventaireComponent } from './inventaire/inventaire.component';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const routes: Routes = [
  { path: 'gestion-de-stock', component: GestionDeStockComponent },
  { path: 'inventaire', component: InventaireComponent },

];

@NgModule({
  declarations: [GestionDeStockComponent, InventaireComponent],
  imports: [
    CommonModule ,
    NgbModule,
    RouterModule.forChild(routes),
  ]
  
})
export class StockModule { }
