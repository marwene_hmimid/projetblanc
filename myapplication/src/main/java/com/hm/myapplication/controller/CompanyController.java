package com.hm.myapplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hm.myapplication.dto.CompanyDTO;
import com.hm.myapplication.dto.UserDTO;
import com.hm.myapplication.service.CompanyService;

@RestController
@CrossOrigin()
@RequestMapping("/api")
public class CompanyController {
	
	@Autowired
	private CompanyService companyService;
	
	
	@PostMapping("/company")
    public ResponseEntity<CompanyDTO> create(@RequestBody CompanyDTO companyDTO) {
		companyService.addCompany(companyDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(companyDTO);
    }

}
