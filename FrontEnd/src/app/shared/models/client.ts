export interface Product {
    id?:string;
    code?:string;
    name?:string;
    description?:string;
    price?:number;
    quantity?:number;
    inventoryStatus?:string;
    category?:string;
    image?:string;
    rating?:number;
}


export interface Product {
    id?:string;
    firstName?:string;
    lastName?:string;
    company?:string;
    email?:string;
    telephone?:string;
    siteInternet?:string;
    fiscal?:string;
    activite?:string;
    devise?:string;
    adress?:string;
    etat?:string;
    codePostal?:string;
    pays?:string;
}