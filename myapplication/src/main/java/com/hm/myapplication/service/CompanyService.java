package com.hm.myapplication.service;

import com.hm.myapplication.dto.CompanyDTO;

public interface CompanyService {
	
	public void addCompany (CompanyDTO companyDTO);

}
