package com.hm.myapplication.serviceImpl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hm.myapplication.dto.CompanyDTO;
import com.hm.myapplication.entities.CompanyEntity;
import com.hm.myapplication.helper.ModelMapperConverter;
import com.hm.myapplication.repository.CompanyRepository;
import com.hm.myapplication.service.CompanyService;

@Service
@Transactional
public class CompanyServiceImpl implements CompanyService{

	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private ModelMapperConverter modelMapperConverter;
 
	
	@Override
	public void addCompany(CompanyDTO companyDTO) {
		companyRepository.save(modelMapperConverter.converToEntity(companyDTO, CompanyEntity.class));
	}

}
