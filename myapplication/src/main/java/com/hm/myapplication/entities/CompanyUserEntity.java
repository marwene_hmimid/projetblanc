package com.hm.myapplication.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class CompanyUserEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;
	
    @ManyToOne
    UserEntity userEntity;

    @ManyToOne
    
    CompanyEntity companyEntity;
    
    int ref;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}

	public CompanyEntity getCompanyEntity() {
		return companyEntity;
	}

	public void setCompanyEntity(CompanyEntity companyEntity) {
		this.companyEntity = companyEntity;
	}

	public int getRef() {
		return ref;
	}

	public void setRef(int ref) {
		this.ref = ref;
	}

	public CompanyUserEntity(Long id, UserEntity userEntity, CompanyEntity companyEntity, int ref) {
		super();
		this.id = id;
		this.userEntity = userEntity;
		this.companyEntity = companyEntity;
		this.ref = ref;
	}

	public CompanyUserEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

    
	
    
	

}
