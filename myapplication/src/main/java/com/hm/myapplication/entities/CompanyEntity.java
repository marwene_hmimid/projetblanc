package com.hm.myapplication.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "company")
@Inheritance(strategy = InheritanceType.JOINED)

public class CompanyEntity extends MyEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String companyName;
	private String address;
	private Double taxIdentificationNumber;
	

	
    @OneToMany(mappedBy = "companyEntity")
    Set<CompanyUserEntity> companyUserEntity;
	
	public CompanyEntity(Long id, String companyName, String address, Double taxIdentificationNumber,
		Set<CompanyUserEntity> companyUserEntity) {
	super();
	this.id = id;
	this.companyName = companyName;
	this.address = address;
	this.taxIdentificationNumber = taxIdentificationNumber;
	this.companyUserEntity = companyUserEntity;
}

	public Set<CompanyUserEntity> getCompanyUserEntity() {
		return companyUserEntity;
	}

	public void setCompanyUserEntity(Set<CompanyUserEntity> companyUserEntity) {
		this.companyUserEntity = companyUserEntity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getTaxIdentificationNumber() {
		return taxIdentificationNumber;
	}

	public void setTaxIdentificationNumber(Double taxIdentificationNumber) {
		this.taxIdentificationNumber = taxIdentificationNumber;
	}

 



 

	public CompanyEntity() {
		super();
 	}



 
	
}
