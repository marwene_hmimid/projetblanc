package com.hm.myapplication.dto;

import java.io.Serializable;
 

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticateDTO extends MyDTO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8941630156100951973L;
	private String username;
	private String token;
	private boolean isauthnenticate;
	 private String message;
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
	

}
