package com.hm.myapplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hm.myapplication.config.JwtTokenProvider;
import com.hm.myapplication.dto.AuthenticateDTO;
import com.hm.myapplication.entities.UserEntity;
import com.hm.myapplication.repository.UserRepository;
import com.hm.myapplication.serviceImpl.CustomUserDetailsService;

 

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	JwtTokenProvider jwtTokenProvider;

	@Autowired
	private CustomUserDetailsService userService;

	/*
	 * login authentication
	 */
	@SuppressWarnings("rawtypes")
	@PostMapping("/login")
	public AuthenticateDTO login(@RequestBody AuthBody data) {
		AuthenticateDTO auth = new AuthenticateDTO();
		auth.setIsauthnenticate(false);
		try {
			String username = data.getEmail();

			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, data.getPassword()));
			String token = jwtTokenProvider.createToken(username, this.userRepository.findByEmail(username).getRoles());

			auth.setToken(token);
			auth.setUsername(username);
			auth.setIsauthnenticate(true);
			auth.setMessage("ok");
			return auth;

		} catch (AuthenticationException e) {

			return auth;
		}

	}

	/*
	 * register authentication
	 */
	@SuppressWarnings("rawtypes")
	@PostMapping("/register")
	public Boolean register(@RequestBody UserEntity user) {
		UserEntity userExists = userService.findUserByEmail(user.getEmail());
		if (userExists != null) {
			return false;
		}
		userService.saveUser(user);
		return true;
	}

}
