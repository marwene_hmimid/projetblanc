package com.hm.myapplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hm.myapplication.dto.UserDTO;
import com.hm.myapplication.entities.UserEntity;
import com.hm.myapplication.service.UserService;

@RestController
@CrossOrigin()
@RequestMapping("/api")
public class UserController {
	
	@Autowired
	private UserService userService;

	@GetMapping("/users")
	public ResponseEntity<List<UserDTO>> getAllUsers() {
		return ResponseEntity.ok(userService.findAllUsers());
	}
 
	@PostMapping("/user")
    public ResponseEntity<UserDTO> create(@RequestBody UserDTO userDTO) {
    	userService.addUser(userDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(userDTO);
    }
	
	@GetMapping("/managers")
	public ResponseEntity<List<UserEntity>> getAllManagers() {
		return ResponseEntity.ok(userService.getAllManagers());
	}
	
}
