package com.hm.myapplication.dto;

import java.io.Serializable;

import com.hm.myapplication.entities.Sexe;
 
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO extends MyDTO implements Serializable{
	
	private Long Id;
	private String email;
	private String password;
	private String firstName;
	private String lastName;
	private String username;
}