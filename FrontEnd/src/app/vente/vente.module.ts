import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DevisComponent } from './devis/devis.component';
import { CommandeClientComponent } from './commande-client/commande-client.component';
import { BonDeLivraisonComponent } from './bon-de-livraison/bon-de-livraison.component';
import { FactureComponent } from './facture/facture.component';
import { BonDeSortieComponent } from './bon-de-sortie/bon-de-sortie.component';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


const routes: Routes = [
  { path: 'devis', component: DevisComponent },
  { path: 'commande-client', component: CommandeClientComponent },
  { path: 'bon-de-livraison', component: BonDeLivraisonComponent },
  { path: 'facture', component: FactureComponent },
  { path: 'bon-de-sortie', component: BonDeSortieComponent },


];
@NgModule({
  declarations: [DevisComponent, CommandeClientComponent, BonDeLivraisonComponent, FactureComponent, BonDeSortieComponent],
  imports: [
    CommonModule,
    NgbModule,
    RouterModule.forChild(routes),

  ]
})
export class VenteModule { }
