import { TestBed } from '@angular/core/testing';

import { FrsService } from './frs.service';

describe('FrsService', () => {
  let service: FrsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FrsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
