export interface Frs {
    id?:string;
    reference?:string;
    name?:string;
    mobileNumber?:number;
    fixNumber?:number;
    creationDate?:Date;
    ca?:number;
    fiscalNumber?:string;
    website?:string;
    email?:string;
    country?:string;
    region?:string;
    postCode?:number;
    adress?:string;

}