import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Frs } from '../models/frs';

@Injectable({
  providedIn: 'root'
})
export class FrsService {




  constructor(private http: HttpClient) { }


  getSuppliers() {
      return this.http.get<any>('assets/json/frs.json')
      .toPromise()
      .then(res => <Frs[]>res.data)
      .then(data => { return data; });
  }

}
