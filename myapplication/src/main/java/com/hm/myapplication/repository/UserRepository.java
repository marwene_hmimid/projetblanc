package com.hm.myapplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hm.myapplication.dto.UserDTO;
import com.hm.myapplication.entities.UserEntity;
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
 	UserEntity findByEmail(String email);
	UserEntity findByFirstName(String firstName);
	
 
	@Query("SELECT a FROM UserEntity a WHERE isManager = true")
	List<UserEntity> findAllManager();
}
