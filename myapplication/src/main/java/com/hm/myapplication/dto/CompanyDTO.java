package com.hm.myapplication.dto;

import java.io.Serializable;

import com.hm.myapplication.entities.UserEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDTO extends MyDTO implements Serializable {

	private Long id;
	private String companyName;
	private String address;
	private Double taxIdentificationNumber;
	private UserEntity user;
	
}
