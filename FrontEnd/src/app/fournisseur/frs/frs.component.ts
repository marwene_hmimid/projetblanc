import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { Frs } from '../../shared/models/frs';
import { Product } from '../../shared/models/product';
import { FrsService } from '../../shared/services/frs.service';
import { ProductService } from '../../shared/services/productservice';

export enum PageNames {
    bloc1,
    bloc2,
    bloc3
  }

  

@Component({
  selector: 'app-frs',
  templateUrl: './frs.component.html',
  styleUrls: ['./frs.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FrsComponent implements OnInit {
  
  fournisseurDialog: boolean;
  fournisseurs: Frs[];
  fournisseur: Frs;
  selectedFournisseurs: Frs[];
  submitted: boolean;
  

  // steps

  items: MenuItem[];
  activeIndex: number = 1;

  ////////////////////////////////::

  PageNames = PageNames;
  dialogPageIndex = PageNames.bloc1;
  
  diaologPages: MenuItem[] = [
    {
      label: "Bloc 1",
      command: (event: any) => { }
    },
    {
      label: "Bloc 2",
      command: (event: any) => { }
    },
    {
        label: "Bloc 3",
        command: (event: any) => { }
      }
  ];

  selectedState: any = null;

  states: any[] = [
      {name: 'Arizona', code: 'Arizona'},
      {name: 'California', value: 'California'},
      {name: 'Florida', code: 'Florida'},
      {name: 'Ohio', code: 'Ohio'},
      {name: 'Washington', code: 'Washington'}
  ];

  cities1: any[] = [];
  
  cities2: any[] = [];
  
  city1:any = null;

  city2:any = null;
  constructor(private frsService: FrsService, private messageService: MessageService, private confirmationService: ConfirmationService, private fb: FormBuilder) { }

  ngOnInit() {
      this.frsService.getSuppliers().then(data => this.fournisseurs = data);

  

// steps 
this.items = [{
    label: 'Personal',
    command: (event: any) => {
        this.activeIndex = this.dialogPageIndex;
    },
 },
{
    label: 'Seat',
    command: (event: any) => {
        this.activeIndex = this.dialogPageIndex;
    },
 },
{
    label: 'Payment',
    command: (event: any) => {
        this.activeIndex = this.dialogPageIndex;
    }
    
},

];
}    
next() {
    this.dialogPageIndex++;
  }
  previous() {
    this.dialogPageIndex--;
  }

  

  openNew() {
      this.fournisseur = {};
      this.submitted = false;
      this.fournisseurDialog = true;
  }

  deleteSelectedfournisseurs() {
      this.confirmationService.confirm({
          message: 'Are you sure you want to delete the selected products?',
          header: 'Confirm',
          icon: 'pi pi-exclamation-triangle',
          accept: () => {
              this.fournisseurs = this.fournisseurs.filter(val => !this.selectedFournisseurs.includes(val));
              this.selectedFournisseurs = null;
              this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
          }
      });
  }

  editFournisseur(fournisseur: Frs) {
      this.fournisseur = {...fournisseur};
      this.fournisseurDialog = true;
  }

  deleteFournisseur(fournisseur: Frs) {
      this.confirmationService.confirm({
          message: 'Are you sure you want to delete ' + fournisseur.name + '?',
          header: 'Confirm',
          icon: 'pi pi-exclamation-triangle',
          accept: () => {
              this.fournisseurs = this.fournisseurs.filter(val => val.id !== fournisseur.id);
            //   this.fournisseur = {};
              this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Deleted', life: 3000});
          }
      });
  }

  hideDialog() {
      this.fournisseurDialog = false;
      this.submitted = false;
  }
  
  saveFournisseur() {
      this.submitted = true;

      if (this.fournisseur.name.trim()) {
          if (this.fournisseur.id) {
              this.fournisseurs[this.findIndexById(this.fournisseur.id)] = this.fournisseur;                
              this.messageService.add({severity:'success', summary: 'Successful', detail: 'bbb Updated', life: 3000});
          }
          else {
              this.fournisseur.id = this.createId();
            //   this.fournisseur.reference = this.createReference();

               this.fournisseurs.push(this.fournisseur);
              this.messageService.add({severity:'success', summary: 'Successful', detail: 'bbbb Created', life: 3000});
          }

          this.fournisseurs = [...this.fournisseurs];
          this.fournisseurDialog = false;
        //   this.fournisseur = {};
      }
  }

  findIndexById(id: string): number {
      let index = -1;
      for (let i = 0; i < this.fournisseurs.length; i++) {
          if (this.fournisseurs[i].id === id) {
              index = i;
              break;
          }
      }

      return index;
  }

  createId(): string {
      let id = '';
      var chars = '0123456789';
      for ( var i = 0; i < 5; i++ ) {
          id += chars.charAt(Math.floor(Math.random() * chars.length));
      }
      return id;
  }
  createReference(): string {
    let id = '';
    var chars = '0123456789';
    for ( var i = 0; i < 5; i++ ) {
        id += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return "f-"+ id;
}
}
